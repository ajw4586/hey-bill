package com.heybill.heybill;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class ItemSpaceDecor extends RecyclerView.ItemDecoration {


    private int space = 0;

    public ItemSpaceDecor(int space) {
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = space;
        if(parent.getChildAdapterPosition(view) == 0)
            outRect.top = space;
    }
}
