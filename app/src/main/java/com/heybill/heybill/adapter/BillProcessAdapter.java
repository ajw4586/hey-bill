package com.heybill.heybill.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.heybill.heybill.R;
import com.heybill.heybill.network.request.RequestService;
import com.heybill.heybill.network.response.BaseResponse;
import com.heybill.heybill.util.BaseImageLoader;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 10. 10..
 */
public class BillProcessAdapter extends RecyclerView.Adapter {

    public class ProcessBillHolder extends RecyclerView.ViewHolder {
        public NetworkImageView bill_img;

        public ProcessBillHolder(View itemView) {
            super(itemView);
            bill_img = (NetworkImageView) itemView.findViewById(R.id.item_bill_img);
        }
    }

    private ImageLoader imageLoader;
    private Context mContext;

    private List<BaseResponse> data;

    public void updateData(List<BaseResponse> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public BillProcessAdapter(Context context) {
        mContext = context;
        imageLoader = BaseImageLoader.getInstance(context).getImageLoader();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case 0:
                itemView = inflater.inflate(R.layout.item_bill_process, null);
                holder = new ProcessBillHolder(itemView);
                break;
            case 1:
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0:
                ProcessBillHolder bill = (ProcessBillHolder) holder;
                bill.bill_img.setImageUrl(RequestService.makeBillImgReq(data.get(position).id), imageLoader);
                break;
            case 1:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }
}
