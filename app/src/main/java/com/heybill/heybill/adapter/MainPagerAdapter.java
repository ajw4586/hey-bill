package com.heybill.heybill.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.heybill.heybill.BillListFragment;
import com.heybill.heybill.BillProcessFragment;
import com.heybill.heybill.PersonFragment;
import com.heybill.heybill.UseageFragment;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class MainPagerAdapter extends FragmentPagerAdapter {


    private BillListFragment billListFragment;
    private BillProcessFragment billProcessFragment;


    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment item = billListFragment;
        switch (position) {
            case 0:
                item = new BillListFragment();
                break;
            case 1:
                item = new BillProcessFragment();
                break;
            case 2:
                item = new UseageFragment();
                break;
            case 3:
                item = new PersonFragment();
                break;
        }
        return item;
    }



    @Override
    public int getCount() {
        return 4;
    }

}
