package com.heybill.heybill.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.heybill.heybill.DetailActivity;
import com.heybill.heybill.R;
import com.heybill.heybill.network.request.BaseRequest;
import com.heybill.heybill.network.request.RequestService;
import com.heybill.heybill.network.response.BaseResponse;
import com.heybill.heybill.util.BaseImageLoader;
import com.heybill.heybill.util.Utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class BillAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ImageLoader imageLoader;
    private Context mContext;

    private List<BaseResponse> data;

    public class DayHolder extends RecyclerView.ViewHolder {
        TextView day;

        public DayHolder(View itemView) {
            super(itemView);
            day = (TextView) itemView.findViewById(R.id.item_day_text);
        }
    }

    public class BillHolder extends RecyclerView.ViewHolder {

        NetworkImageView bill_img;
        TextView place, amount, period, method, category;
        ImageView category_img;

        public BillHolder(View itemView) {
            super(itemView);
            bill_img = (NetworkImageView) itemView.findViewById(R.id.item_bill_img);
            place = (TextView) itemView.findViewById(R.id.item_bill_place);
            amount = (TextView) itemView.findViewById(R.id.item_bill_pay_amount);
            period = (TextView) itemView.findViewById(R.id.item_bill_pay_period);
            method = (TextView) itemView.findViewById(R.id.item_bill_pay_method);
            category = (TextView) itemView.findViewById(R.id.item_bill_category);
            category_img = (ImageView) itemView.findViewById(R.id.item_bill_category_img);
        }
    }

    public BillAdapter(Context context) {
        mContext = context;
        imageLoader = BaseImageLoader.getInstance(context).getImageLoader();
    }

    public void updateData(List<BaseResponse> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RecyclerView.ViewHolder holder = null;
        View itemVIew;
        switch (viewType) {
            case 0:
                itemVIew = inflater.inflate(R.layout.item_bill, null);
                holder = new BillHolder(itemVIew);
                break;
            case 1:
                break;
        }
        return holder;
    }

    String formatCurrency(int currency) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        DecimalFormat format = new DecimalFormat("#,###");
        return format.format(currency);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case 0:
                BillHolder bill = (BillHolder) holder;
                bill.bill_img.setImageUrl(RequestService.makeBillImgReq(data.get(position).id), imageLoader);
                bill.place.setText(data.get(position).vendor);
                bill.category.setText(data.get(position).category);
                bill.method.setText(data.get(position).payMethod);
                bill.period.setText(Utils.convertTimeStamp(data.get(position).transactionTime));
                bill.amount.setText(formatCurrency(data.get(position).payAmount));
                if(data.get(position).category != null)
                    bill.category_img.setImageResource(getMatchCategory(data.get(position).category));
                bill.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, DetailActivity.class);
                        intent.putExtra("id", data.get(position).id);
                        intent.putExtra("place", data.get(position).vendor);
                        intent.putExtra("method", data.get(position).payMethod);
                        intent.putExtra("date", Utils.convertTimeStamp(data.get(position).transactionTime));
                        intent.putExtra("category", data.get(position).category);
                        int item_size = data.get(position).items.size();
                        intent.putExtra("item_count", item_size);
                        for(int i = 0; i < item_size; i++) {
                            intent.putExtra("item" + i, data.get(position).items.get(i).name);
                            intent.putExtra("item_price" + i, data.get(position).items.get(i).price);
                            intent.putExtra("item_qty" + i, data.get(position).items.get(i).qty);
                        }
                        mContext.startActivity(intent);
                    }
                });
                break;
            case 1:
                break;
        }
    }

    int getMatchCategory(String category) {
        int[] ids = new int[]{
            R.drawable.ic_category_01_foods,
            R.drawable.ic_category_02_books,
            R.drawable.ic_category_03_oiling,
            R.drawable.ic_category_04_cinema
        };
        int result = 0;
        if(category.equals("주유비")) {
            result = ids[2];
        } else if(category.equals("식비")) {
            result = ids[0];
        } else if(category.equals("문화생활")) {
            result = ids[3];
        } else if(category.equals("교재구입")) {
            result = ids[1];
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
}
