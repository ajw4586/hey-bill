package com.heybill.heybill;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.heybill.heybill.adapter.BillAdapter;
import com.heybill.heybill.adapter.BillProcessAdapter;
import com.heybill.heybill.adapter.MainPagerAdapter;
import com.heybill.heybill.network.request.RequestService;
import com.heybill.heybill.network.response.BaseResponse;

import java.net.InterfaceAddress;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {


    private ViewPager mViewPager;
    private MainPagerAdapter adapter;

    private ActionBarDrawerToggle toggle;
    private DrawerLayout menuDrawer;

    private TabLayout tabs;

    private RequestService.RequestInterface requestInterface = RequestService.getInstance();
    private boolean needUpdate = false;

    public BillAdapter billAdapter;
    public BillProcessAdapter billProcessAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        initView();
    }

    void init() {
        adapter = new MainPagerAdapter(getSupportFragmentManager());
        billAdapter = new BillAdapter(this);
        billProcessAdapter = new BillProcessAdapter(this);
    }

    void initView() {
        menuDrawer = (DrawerLayout) findViewById(R.id.main_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.post_bill);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SendBill.class));
            }
        });
        toggle = new ActionBarDrawerToggle(this, menuDrawer, toolbar, R.string.blank, R.string.blank);

        //  pager setup
        mViewPager = (ViewPager) findViewById(R.id.main_pager);
        mViewPager.setAdapter(adapter);

        //  Tab setup
        tabs = (TabLayout) findViewById(R.id.main_tab);
        tabs.setupWithViewPager(mViewPager);
        tabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d(getClass().getSimpleName(), "Tab selected: " + tab.getPosition());
                if (tab.getPosition() != 0) {
                    if (fab.getVisibility() == View.VISIBLE)
                        fab.setVisibility(View.GONE);
                } else {
                    if (fab.getVisibility() == View.GONE)
                        fab.setVisibility(View.VISIBLE);
                }
                super.onTabSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setUpTab();
        refresh();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    void setUpTab() {
        TabLayout.Tab tab1 = tabs.getTabAt(0);
        TabLayout.Tab tab2 = tabs.getTabAt(1);
        TabLayout.Tab tab3 = tabs.getTabAt(2);
        TabLayout.Tab tab4 = tabs.getTabAt(3);
        tab1.setIcon(R.drawable.tab1);
        tab2.setIcon(R.drawable.tab2);
        tab3.setIcon(R.drawable.tab3);
        tab4.setIcon(R.drawable.tab4);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(needUpdate)
            refresh();
    }

    @Override
    protected void onStop() {
        super.onStop();
        needUpdate = true;
    }

    public void refresh() {
        requestInterface.getBills(new Callback<List<BaseResponse>>() {
            @Override
            public void success(List<BaseResponse> baseResponses, Response response) {
                Log.d(getClass().getSimpleName(), "success response.");
                sortData(baseResponses);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    void sortData(List<BaseResponse> data) {
        List<BaseResponse> process = new ArrayList<BaseResponse>();
        List<BaseResponse> not_process = new ArrayList<BaseResponse>();

        for(BaseResponse res : data) {
            if(res.status.equals("P")) {
                process.add(res);
            } else if(res.status.equals("N")) {
                not_process.add(res);
            }
        }
        Log.d(getClass().getSimpleName(), process.size() + " " + not_process.size());
        billAdapter.updateData(process);
        billProcessAdapter.updateData(not_process);
    }

}
