package com.heybill.heybill;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heybill.heybill.adapter.BillAdapter;
import com.heybill.heybill.network.request.RequestService;
import com.heybill.heybill.network.response.BaseResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillListFragment extends Fragment {


    private View rootView;

    private RecyclerView bill_list;
    private LinearLayoutManager llistManager;
    private BillAdapter adapter;

    private boolean needRefresh = false;

    private RequestService.RequestInterface requestInterface = RequestService.getInstance();

    private List<BaseResponse> data;

    public BillListFragment() {
        // Required empty public constructor
    }

    public void setAdapter(BillAdapter adapter) {
        this.adapter = adapter;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_bill_list, container, false);
        init();
        initView();
        return rootView;
    }

    void init() {
        llistManager = new LinearLayoutManager(getContext());
    }

    void initView() {
        bill_list = (RecyclerView) rootView.findViewById(R.id.bill_list);
        bill_list.setLayoutManager(llistManager);
        bill_list.addItemDecoration(new ItemSpaceDecor(10));
        bill_list.setAdapter(((MainActivity)getActivity()).billAdapter);
    }


    public void setData(List<BaseResponse> data) {
        this.data = data;
        adapter.updateData(data);
    }
}
