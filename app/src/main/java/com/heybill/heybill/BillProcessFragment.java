package com.heybill.heybill;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heybill.heybill.adapter.BillProcessAdapter;
import com.heybill.heybill.network.response.BaseResponse;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillProcessFragment extends Fragment {


    private View rootView;

    private RecyclerView process_list;
    private LinearLayoutManager llManager;

    private BillProcessAdapter adapter;

    private List<BaseResponse> data;

    public BillProcessFragment() {
        // Required empty public constructor
    }


    public void setAdapter(BillProcessAdapter adapter) {
        this.adapter = adapter;
    }

    public void setData(List<BaseResponse> data) {
        this.data = data;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_bill_process, container, false);
        init();
        initView();
        return rootView;
    }

    void init() {
        llManager = new LinearLayoutManager(getContext());
        adapter = new BillProcessAdapter(getActivity());
    }

    void initView() {
        process_list = (RecyclerView) rootView.findViewById(R.id.bill_process_list);
        process_list.setLayoutManager(llManager);
        process_list.setAdapter(((MainActivity)getActivity()).billProcessAdapter);
    }


}
