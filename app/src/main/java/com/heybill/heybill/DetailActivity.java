package com.heybill.heybill;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.heybill.heybill.network.request.RequestService;
import com.heybill.heybill.util.BaseImageLoader;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DetailActivity extends AppCompatActivity {

    private TextView place, method, date, item, item_price, item_price_sum;
    private ImageView category_img;
    private NetworkImageView bill_img;

    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initView();
        getInfo();
    }

    void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().
                getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));

        imageLoader = BaseImageLoader.getInstance(this).getImageLoader();
        place = (TextView) findViewById(R.id.detail_place);
        method = (TextView) findViewById(R.id.detail_bill_method);
        date = (TextView) findViewById(R.id.detail_bill_date);
        item = (TextView) findViewById(R.id.detail_bill_item);
        item_price = (TextView) findViewById(R.id.detail_bill_item_money);
        item_price_sum = (TextView) findViewById(R.id.detail_bill_item_sum);
        category_img = (ImageView) findViewById(R.id.detail_category_img);
        bill_img = (NetworkImageView) findViewById(R.id.detail_bill_img);
    }

    void getInfo() {
        Intent info = getIntent();
        String id = info.getStringExtra("id");
        String category = info.getStringExtra("category");
        String place = info.getStringExtra("place");
        String method = info.getStringExtra("method");
        String date = info.getStringExtra("date");
        int count = info.getIntExtra("item_count", 0);
        int price_sum = 0;
        StringBuilder item_sb = new StringBuilder();
        StringBuilder item_price_sb = new StringBuilder();
        for(int i = 0; i < count; i++) {
            String item = info.getStringExtra("item" + i);
            int item_price = info.getIntExtra("item_price" + i, 0);
            int item_qty = info.getIntExtra("item_qty" + i, 0);
            item_sb.append(item);
            item_price_sb.append(formatCurrency(item_price * item_qty));
            price_sum += item_price * item_qty;
            //  줄바꿈
            if(i -1 != count) {
                item_sb.append("\n\n");
                item_price_sb.append("\n\n");
            }
        }
        this.item.setText(item_sb.toString());
        this.item_price.setText(item_price_sb.toString());
        this.item_price_sum.setText(formatCurrency(price_sum));
        this.place.setText(place);
        this.method.setText(method);
        this.date.setText(date);
        this.category_img.setImageResource(getMatchCategory(category));
        this.bill_img.setImageUrl(RequestService.makeBillImgReq(id), imageLoader);
    }

    String formatCurrency(int currency) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        DecimalFormat format = new DecimalFormat("#,###");
        return format.format(currency);
    }


    int getMatchCategory(String category) {
        int[] ids = new int[]{
                R.drawable.ic_category_01_foods,
                R.drawable.ic_category_02_books,
                R.drawable.ic_category_03_oiling,
                R.drawable.ic_category_04_cinema
        };
        int result = 0;
        if(category.equals("주유비")) {
            result = ids[2];
        } else if(category.equals("식비")) {
            result = ids[0];
        } else if(category.equals("문화생활")) {
            result = ids[3];
        } else if(category.equals("교재구입")) {
            result = ids[1];
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
