package com.heybill.heybill.network.request;


import com.heybill.heybill.network.response.BaseResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class RequestService {

    public interface RequestInterface {

        @Multipart
        @POST("/bills")
        public void uploadBillPic(@Part("image")TypedFile billPic, Callback<String> cb);

        @GET("/bills")
        public void getBills(Callback<List<BaseResponse>> callback);
    }

    public static String makeBillImgReq(String id) {
        return BaseRequest.ENDPOINT + "/uploaded/images/" + id;
    }

    public static RequestInterface getInstance() {
        return BaseRequest.getInstance().getBaseAdapter().create(RequestInterface.class);
    }
}
