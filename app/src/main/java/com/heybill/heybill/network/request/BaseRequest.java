package com.heybill.heybill.network.request;

import retrofit.RestAdapter;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class BaseRequest {

    public static final String ENDPOINT = "http://localhost:8080";

    private static BaseRequest instance;
    private static RestAdapter baseAdapter;

    public static BaseRequest getInstance() {
        return instance != null ? instance : new BaseRequest();
    }

    protected BaseRequest() {
        baseAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    public RestAdapter getBaseAdapter() {
        return baseAdapter;
    }

}
