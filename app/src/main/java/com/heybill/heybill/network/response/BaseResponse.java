package com.heybill.heybill.network.response;

import java.util.List;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class BaseResponse {

    public String id;
    public String vendor;
    public String telephone;
    public String address;
    //  금액 / 수단 / 시기
    public int payAmount;
    public String payMethod;
    public String payPeriod;
    //
    public String transactionTime;
    //  지출 유형
    public String category;
    public String status;
    public List<Items> items;

    public class Items {
        public String id;
        public String name;
        public int qty;
        public int price;
    }
}
