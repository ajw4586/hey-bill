package com.heybill.heybill;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.heybill.heybill.camera.Preview;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * A placeholder fragment containing a simple view.
 */
public class CameraFragment extends Fragment {


    public static final String DIR_PATH = "/hey_bill/";
    public static final String FILE_NAME = "bill.jpg";

    private View rootView;

    private Preview preview;
    private Button take;
    private ViewGroup previewGroup;

    public CameraFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_camera, container, false);
        initView();
        return rootView;
    }

    void initView() {
        previewGroup = (ViewGroup) rootView.findViewById(R.id.camera_preview);
        take = (Button) rootView.findViewById(R.id.camera_take);

        take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                take(new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes, Camera camera) {
                        //  Saving photo data

                        //  create dir if it isn't exists
                        File path = new File(Environment.getExternalStorageDirectory(), DIR_PATH);
                        if (!path.exists())
                            path.mkdirs();
                        FileOutputStream fos = null;
                        try {

                            //  save photo
                            File dest = new File(path, FILE_NAME);
                            fos = new FileOutputStream(dest);

                            long t = System.currentTimeMillis();
                            Matrix rotate = new Matrix();
                            rotate.postRotate(90);
                            Bitmap imageSrc = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            Bitmap scaled = Bitmap.createScaledBitmap(imageSrc, imageSrc.getWidth() / 3, imageSrc.getHeight() / 3, true);
                            imageSrc = Bitmap.createBitmap(scaled, 0, 0, scaled.getWidth(), scaled.getHeight(), rotate, false);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            imageSrc.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            bytes = stream.toByteArray();

                            fos.write(bytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "오류가 발생했습니다", Toast.LENGTH_SHORT).show();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            ((SendBill) getActivity()).callResult(bytes);
                        }
                    }
                });
            }
        });

    }


    public void take(Camera.PictureCallback callback) {
        preview.takePicture(callback);
    }

    @Override
    public void onResume() {
        super.onResume();
        preview = new Preview(getActivity(), 0);
        previewGroup.addView(preview);
    }

    @Override
    public void onPause() {
        super.onPause();
        previewGroup.removeView(preview);
    }
}
