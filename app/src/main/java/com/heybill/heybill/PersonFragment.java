package com.heybill.heybill;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonFragment extends Fragment {


    private View rootView;

    private CircleImageView profile_img;

    public PersonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_person, container, false);
        initView();
        return rootView;
    }

    void initView() {
        profile_img = (CircleImageView) rootView.findViewById(R.id.person_img);
        profile_img.setBorderWidth(3);
        profile_img.setBorderColor(Color.WHITE);
    }


}
