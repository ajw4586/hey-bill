package com.heybill.heybill.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class BaseImageLoader {

    public static BaseImageLoader instance;

    public static com.android.volley.toolbox.ImageLoader sImageLoader;
    public static RequestQueue sRequestQueue;

    public static BaseImageLoader getInstance(Context context) {
        return instance != null ? instance : new BaseImageLoader(context);
    }

    private BaseImageLoader(Context context) {
        sRequestQueue = Volley.newRequestQueue(context);
        sImageLoader = new com.android.volley.toolbox.ImageLoader(sRequestQueue, new com.android.volley.toolbox.ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> cache = new LruCache<>(20);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    public com.android.volley.toolbox.ImageLoader getImageLoader() {
        return sImageLoader;
    }

    public RequestQueue getRequestQueue() {
        return sRequestQueue;
    }
}
