package com.heybill.heybill.util;

import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class Utils {

    public static String convertTimeStamp(String timeStamp) {
        long stamp = Long.valueOf(timeStamp);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(stamp);
        String date = DateFormat.format("yy.mm.dd", cal).toString();
        return date;
    }
}
