package com.heybill.heybill;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SendBill extends AppCompatActivity {


    public static final String DIR_PATH = "/hey_bill/";
    public static final String FILE_NAME = "bill.jpg";


    private FragmentManager mFManager;

    private CameraFragment cameraFragment;
    private PhotoResultFragment photoResultFragment;


    private Button take;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_bill);
        init();
        initView();
    }

    void initView() {
        mFManager.beginTransaction()
                .add(R.id.fragment, cameraFragment)
                .commit();

    }

    void init() {
        mFManager = getSupportFragmentManager();
        cameraFragment = new CameraFragment();
    }

    boolean checkSaved() {
        File path = new File(Environment.getExternalStorageDirectory(), DIR_PATH + FILE_NAME);
        return path.exists();
    }

    boolean deletePhoto() {
        File path = new File(Environment.getExternalStorageDirectory(), DIR_PATH + FILE_NAME);
        if(path.exists())
            return path.delete();
        return false;
    }

    public void callCamera() {
        mFManager.beginTransaction()
                .replace(R.id.fragment, cameraFragment)
                .commit();
    }

    public void callResult(byte[] data) {
        photoResultFragment = new PhotoResultFragment();
        photoResultFragment.setImageData(data);
        mFManager.beginTransaction()
                .replace(R.id.fragment, photoResultFragment)
                .commit();
    }


}
