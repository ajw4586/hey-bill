package com.heybill.heybill;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.heybill.heybill.network.request.RequestService;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoResultFragment extends Fragment implements View.OnClickListener {


    private View rootView;

    private TextView retake, use;
    private ImageView image;

    private byte[] imageData;
    private Bitmap imageSrc;


    RequestService.RequestInterface requestInterface = RequestService.getInstance();

    public PhotoResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_photo_result, container, false);
        init();
        initView();
        return rootView;
    }

    void init() {
        if(imageData != null) {
            imageSrc = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        }
    }

    public void setImageData(byte[] data) {
        imageData = data;
    }

    void initView() {
        image = (ImageView) rootView.findViewById(R.id.photo_result_image);
        retake = (TextView) rootView.findViewById(R.id.photo_result_retake);
        use = (TextView) rootView.findViewById(R.id.photo_result_use);
        retake.setOnClickListener(this);
        use.setOnClickListener(this);

        image.setImageBitmap(imageSrc);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.photo_result_retake:
                ((SendBill)getActivity()).callCamera();
                break;
            case R.id.photo_result_use:
                if(((SendBill) getActivity()).checkSaved() && imageData != null) {
                    File path = new File(Environment.getExternalStorageDirectory(), SendBill.DIR_PATH);
                    path = new File(path, SendBill.FILE_NAME);
                    TypedFile data = new TypedFile("image/*", path);
                    requestInterface.uploadBillPic(data, new Callback<String>() {
                        @Override
                        public void success(String aResponse, Response response) {
                            Log.d(getClass().getSimpleName(), response.getBody().toString());
                            getActivity().finish();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity(), "영수증을 업로드하지 못했습니다", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;
        }
    }
}
