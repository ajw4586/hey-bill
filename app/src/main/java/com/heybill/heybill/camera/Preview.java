package com.heybill.heybill.camera;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by globalsmartkr on 2015. 10. 9..
 */
public class Preview extends SurfaceView implements SurfaceHolder.Callback{

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private int cameraId;

    private List<Camera.Size> supportedPreviewSize;
    private Camera.Size mPreviewSize;

    private Activity parent;

    public Preview(Context context) {
        super(context);
    }


    public Preview(Activity activity, int id) {
        super(activity);
        parent = activity;
        mHolder = getHolder();
        mHolder.addCallback(this);

        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        //  create camera
        cameraId = id;

        if(!openCamera(cameraId)) {
            Toast.makeText(getContext(), "카메라를 실행할 수 없습니다", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO 오토포커스 만들기
        return super.onTouchEvent(event);
    }

    protected boolean openCamera(int id) {
        boolean isOpened = false;
        try {
            //  open camera
            if (Camera.getNumberOfCameras() > cameraId) {
                cameraId = id;
            } else {
                cameraId = 0;
            }
            mCamera = Camera.open(cameraId);
            //  roatae camera portrait.
            mCamera.setDisplayOrientation(90);
            supportedPreviewSize = mCamera.getParameters().getSupportedPreviewSizes();
            isOpened = mCamera != null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isOpened;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        try {
            mCamera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        if(supportedPreviewSize != null) {
            Log.d(getClass().getSimpleName(), "onMeasure Called");
            //  Scale the preview size
            Display display = parent.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            mPreviewSize = getOptimalPreviewSize(supportedPreviewSize, size.x, size.y);
            mCamera.getParameters().setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        }
    }

    public void takePicture(Camera.PictureCallback pictureCallback) {
        mCamera.takePicture(null, null, pictureCallback);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int w, int h) {
        mCamera.getParameters().setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        mCamera.startPreview();
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

}
